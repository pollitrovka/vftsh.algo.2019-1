#include <iostream>
using namespace std;

class stack{
    int array[100];
    int count = 0;
public:
    void push(int x){
        array[count] = x;
        count++;
    }
    void pop(){
        if(count != 0){
            count--;
        }
    }
    int top(){
        if(count != 0)
            return array[count - 1];
        else
            return 0;
    }
};
